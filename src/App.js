import React, { Component } from 'react';
import LoginPage from './Containers/Login'
import './App.css';
import Routes from './Containers/Routes'


class App extends Component {
  render() {
    return (
      <div className="App">
        <Routes/>
      </div>
    );
  }
}

export default App;
