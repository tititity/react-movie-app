import React, { Component } from 'react'
import { Spin, Modal, Button, Layout, Menu } from 'antd'
import ListMovie from '../Components/ListMovie'
import RouteMenu from './RouteMenu';

const menus = ['movies', 'favorite', 'profile']
const { Header, Content, Footer } = Layout; //destruct ของใน layout ออกมา?

class Main extends Component {

    state = {
        items: [],
        isShowModal: false,
        itemMovie: null,
        pathName: menus[0]
    }
    componentDidMount() {
        //TODO: get list of movie from api
        const { pathname } = this.props.location;
        var pathName = menus[0]; //initial ว่า pathName จะเท่ากับ movies เสมอ
        if(pathName !== '/'){
            pathName = pathname.replace('/',''); //replace '/' ด้วย '' ex. '/somethhing' -> 'something'
            if(!menus.includes(pathName)) pathName = menus[0]; 
            //includes ช่วยเช็คว่า pathName มีอยู่ใน array ของ menus มั้ย รีเทิร์นเป้น true/false
            //แสดงว่า ถ้า menus ไม่อยู่ในarray เซ็ทให้กลับไปค่าตั้งต้น movies เสมอ
        }
        this.setState({ pathName });
        fetch('https://workshopup.herokuapp.com/movie')
            .then(response => response.json())
            .then(movies => this.setState({ items: movies.results }))
    }

    onItemMovieClick = (item) => {
        //รับค่า item มา set ใส่ ItemMovie
        this.setState({
            isShowModal: true,
            itemMovie: item
        })
    }

    onModalClickOk = () => {
        this.setState({ isShowModal: false })
    }

    onModalClickCancle = () => {
        this.setState({ isShowModal: false })
    }

    onMenuClick = e => {
        var path ='/'
        
        if(e.key !== 'movies'){
            path =`${e.key}`
        }
        this.props.history.replace(path);
    }

    render() {
        const item = this.state.itemMovie;
        return (
            //ใน html ใส่ if else ไม่ได้ต้องใส่ short if แทน
            <div>
                {this.state.items.length > 0 ? (
                    <div style={{ height: '100vh' }}>
                        {' '}
                        <Layout className="layout" style={{ background: 'white' }}>
                            <Header
                                style={{
                                    padding: '0px',
                                    position: 'fixed',
                                    zIndex: 1,
                                    width: '100%'
                                }}
                            >
                                <Menu
                                    theme="light"
                                    mode="horizontal"
                                    defaultSelectedKeys={[this.state.pathName]}
                                    style={{ lineHeight: '64px' }}
                                    onClick={e => {
                                        this.onMenuClick(e);
                                    }}
                                >
                                    <Menu.Item key={menus[0]}>Home</Menu.Item>
                                    <Menu.Item key={menus[1]}>Favorite</Menu.Item>
                                    <Menu.Item key={menus[2]}>Profile</Menu.Item>
                                </Menu>
                            </Header>
                            <Content
                                style={{
                                    padding: '16px',
                                    marginTop: 64,
                                    minHeight: '600px',
                                    justifyContent: 'center', //จัดแนวนอน
                                    alignItems: 'center',//จัดแนวตั้ง
                                    display: 'flex'
                                }}
                            >
                                <RouteMenu
                                    items={this.state.items}
                                    onItemMovieClick={this.onItemMovieClick}
                                />
                            </Content>
                            <Footer style={{ textAlign: 'center', background: 'white' }}>
                                Movie Application Workshop @ CAMT
                        </Footer>
                        </Layout>
                    </div>
                ) : (
                        <Spin size="large" />
                    )}
                {item !== null ? (
                    <Modal
                        width="40%"
                        style={{ maxHeigt: '70%' }}
                        title={this.state.itemMovie.title}
                        visible={this.state.isShowModal}
                        onCancel={this.onModalClickCancle}
                        footer={[
                            <Button
                                key="submit"
                                type="primary"
                                icon="heart"
                                size="large"
                                shape="circle"
                                onClick={this.onClickFavorite} />,
                            <Button
                                key="submit"
                                type="primary"
                                icon="shopping-cart"
                                size="large"
                                shape="circle"
                                onClick={this.onClickBuyTicket} />
                        ]}
                    >
                        <img src={item.image_url} style={{ width: "100%" }} />
                        <br />
                        <br />
                        <p>{item.overview}</p>
                    </Modal>) : (
                        <div />
                    )}
            </div>
        )
    }
}

export default Main;